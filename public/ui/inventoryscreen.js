function equipWeapon(itemIndex){
    tracker.track("Equipped Weapon");
    var character = game.character;
    var item = character.removeItem(itemIndex);
    character.messages.push("You equip the "+getItemName(item)+".");
    item = character.equip("OnHand",item);//TODO:there is more to it, for two handedness and so on!
    character.addItem(item);
    showInventory();
}

function dropItem(itemIndex){
    tracker.track("Dropped Item");
    var character = game.character;
    var branch = getCharacterBranch();
    var item = character.removeItem(itemIndex);
    branch.addItem(item);
    character.messages.push("You drop the "+getItemName(item)+".");
    showInventory();
}

function showInventory(){
    tracker.track("Viewed Inventory");
    var content = "";
    var character = game.character;
    content += "<ul class='list-group'>";
    for(var index in character.inventory){
        var item = character.inventory[index];
        content += "<li class='list-group-item'>"+getItemName(item);
        content += "<a href='#' onclick='dropItem("+index+")'>Drop</a>";
        if(item.itemType=="Weapon"){
            content += "<a href='#' onclick='equipWeapon("+index+")'>Equip</a>";
        }
        content += "</li>"
    }
    content += "</ul>"
    $('#main')
        .empty()
        .append(content);
}

