var weapons = {
    "Club":{
        "cost":0.1,
        "damageRoll": function(character){ return Utility.rollDie(4)+character.getAbilityModifier(this.ability); },
        "damageType": "Bludgeoning",
        "weight":2,
        "ability":"Strength",
        "proficiencies":["SimpleWeapons","Clubs"],
        "hands":1,
        "tags":[]
    },   
    "Dagger":{
        "cost":2,
        "damageRoll": function(character){ return Utility.rollDie(4)+character.getAbilityModifier(this.ability); },
        "damageType": "Piercing",
        "weight":1,
        "ability":"Strength",
        "proficiencies":["SimpleWeapons","Daggers"],
        "hands":1,
        "tags":[]
    },   
    "Greatclub":{
        "cost": 0.2,
        "damageRoll": function(character){ return Utility.rollDie(8)+character.getAbilityModifier(this.ability); },
        "damageType": "Bludgeoning",
        "weight":10,
        "ability":"Strength",
        "proficiencies":["SimpleWeapons","Clubs"],
        "hands":2,
        "tags":[]
    },   
    "Handaxe":{
        "cost": 5,
        "damageRoll": function(character){ return Utility.rollDie(6)+character.getAbilityModifier(this.ability); },
        "damageType": "Slashing",
        "weight":2,
        "ability":"Strength",
        "proficiencies":["SimpleWeapons","Handaxes"],
        "hands":1,
        "tags":[]
    },   
    "Javelin":{
        "cost": 0.5,
        "damageRoll": function(character){ return Utility.rollDie(6)+character.getAbilityModifier(this.ability); },
        "damageType": "Piercing",
        "weight":2,
        "ability":"Strength",
        "proficiencies":["SimpleWeapons","Javelins"],
        "hands":1,
        "tags":[]
    },   
    "Light Hammer":{
        "cost": 2,
        "damageRoll": function(character){ return Utility.rollDie(4)+character.getAbilityModifier(this.ability); },
        "damageType": "Bludgeoning",
        "weight":2,
        "ability":"Strength",
        "proficiencies":["SimpleWeapons","LightHammers"],
        "hands":1,
        "tags":[]
    },   
    "Mace":{
        "cost": 5,
        "damageRoll": function(character){ return Utility.rollDie(6)+character.getAbilityModifier(this.ability); },
        "damageType": "Bludgeoning",
        "weight":4,
        "ability":"Strength",
        "proficiencies":["SimpleWeapons","Maces"],
        "hands":1,
        "tags":[]
    },   
    "Quarterstaff":{
        "cost": 0.2,
        "damageRoll": function(character){ return Utility.rollDie(6)+character.getAbilityModifier(this.ability); },
        "damageType": "Bludgeoning",
        "weight":4,
        "ability":"Strength",
        "proficiencies":["SimpleWeapons","Quarterstaves"],
        "hands":1,
        "tags":[]
    },   
    "Sickle":{
        "cost": 1,
        "damageRoll": function(character){ return Utility.rollDie(4)+character.getAbilityModifier(this.ability); },
        "damageType": "Slashing",
        "weight":2,
        "ability":"Strength",
        "proficiencies":["SimpleWeapons","Sickles"],
        "hands":1,
        "tags":[]
    },   
    "Spear":{
        "cost": 1,
        "damageRoll": function(character){ return Utility.rollDie(6)+character.getAbilityModifier(this.ability); },
        "damageType": "Piercing",
        "weight":3,
        "ability":"Strength",
        "proficiencies":["SimpleWeapons","Spears"],
        "hands":1,
        "tags":[]
    },
    "LightCrossbow":{
        "cost": 25,
        "damageRoll": function(character){ return Utility.rollDie(8)+character.getAbilityModifier(this.ability); },
        "damageType": "Piercing",
        "weight":5,
        "ability":"Dexterity",
        "proficiencies":["SimpleWeapons","Crossbows"],
        "hands":1,
        "tags":[]
    },
    "Dart":{
        "cost": 0.5,
        "damageRoll": function(character){ return Utility.rollDie(4)+character.getAbilityModifier(this.ability); },
        "damageType": "Piercing",
        "weight":0.25,
        "ability":"Dexterity",
        "proficiencies":["SimpleWeapons","Darts"],
        "hands":1,
        "tags":[]
    },
    "Shortbow":{
        "cost": 1,
        "damageRoll": function(character){ return Utility.rollDie(6)+character.getAbilityModifier(this.ability); },
        "damageType": "Piercing",
        "weight":2,
        "ability":"Dexterity",
        "proficiencies":["SimpleWeapons","Shortbows"],
        "hands":1,
        "tags":[]
    },
    "Sling":{
        "cost": 0,
        "damageRoll": function(character){ return Utility.rollDie(4)+character.getAbilityModifier(this.ability); },
        "damageType": "Bludgeoning",
        "weight":0,
        "ability":"Dexterity",
        "proficiencies":["SimpleWeapons","Slings"],
        "hands":1,
        "tags":[]
    },
    "Battleaxe":{
        "cost": 10,
        "damageRoll": function(character){ return Utility.rollDie(8)+character.getAbilityModifier(this.ability); },
        "damageType": "Slashing",
        "weight":4,
        "ability":"Strength",
        "proficiencies":["MartialWeapons","Axes"],
        "hands":1,
        "tags":[]
    },
    "Flail":{
        "cost": 10,
        "damageRoll": function(character){ return Utility.rollDie(8)+character.getAbilityModifier(this.ability); },
        "damageType": "Bludgeoning",
        "weight":2,
        "ability":"Strength",
        "proficiencies":["MartialWeapons","Flails"],
        "hands":1,
        "tags":[]
    },
    "Glaive":{
        "cost": 20,
        "damageRoll": function(character){ return Utility.rollDie(10)+character.getAbilityModifier(this.ability); },
        "damageType": "Slashing",
        "weight":6,
        "ability":"Strength",
        "proficiencies":["MartialWeapons","Polearms"],
        "hands":2,
        "tags":["Heavy","Reach"]
    },
    "Greataxe":{
        "cost": 30,
        "damageRoll": function(character){ return Utility.rollDie(12)+character.getAbilityModifier(this.ability); },
        "damageType": "Slashing",
        "weight":7,
        "ability":"Strength",
        "proficiencies":["MartialWeapons","Axes"],
        "hands":2,
        "tags":["Heavy"]
    },
    "Greatsword":{
        "cost": 50,
        "damageRoll": function(character){ return Utility.rollDie(6)+Utility.rollDie(6); },
        "damageType": "Slashing",
        "weight":6,
        "ability":"Strength",
        "proficiencies":["MartialWeapons","Swords"],
        "hands":2,
        "tags":["Heavy"]
    },
    "Halberd":{
        "cost": 20,
        "damageRoll": function(character){ return Utility.rollDie(10)+character.getAbilityModifier(this.ability); },
        "damageType": "Slashing",
        "weight":6,
        "ability":"Strength",
        "proficiencies":["MartialWeapons","Polearms"],
        "hands":2,
        "tags":["Heavy","Reach"]
    },
    "Lance":{
        "cost": 10,
        "damageRoll": function(character){ return Utility.rollDie(12)+character.getAbilityModifier(this.ability); },
        "damageType": "Piercing",
        "weight":4,
        "ability":"Strength",
        "proficiencies":["MartialWeapons","Lances"],
        "hands":2,
        "tags":[]
    },
    "Longsword":{
        "cost": 15,
        "damageRoll": function(character){ return Utility.rollDie(8)+character.getAbilityModifier(this.ability); },
        "damageType": "Slashing",
        "weight":3,
        "ability":"Strength",
        "proficiencies":["MartialWeapons","Swords"],
        "hands":1.5,
        "tags":[]
    },
    "Maul":{
        "cost": 10,
        "damageRoll": function(character){ return Utility.rollDie(6)+Utility.rollDie(6)+character.getAbilityModifier(this.ability); },
        "damageType": "Bludgeoning",
        "weight":10,
        "ability":"Strength",
        "proficiencies":["MartialWeapons","Mauls"],
        "hands":2,
        "tags":["Heavy"]
    },
    "Morningstar":{
        "cost": 15,
        "damageRoll": function(character){ return Utility.rollDie(8)+character.getAbilityModifier(this.ability); },
        "damageType": "Piercing",
        "weight":4,
        "ability":"Strength",
        "proficiencies":["MartialWeapons","Morningstars"],
        "hands":1,
        "tags":[]
    },
    "Pike":{
        "cost": 15,
        "damageRoll": function(character){ return Utility.rollDie(6)+character.getAbilityModifier(this.ability); },
        "damageType": "Piercing",
        "weight":18,
        "ability":"Strength",
        "proficiencies":["MartialWeapons","Polearms"],
        "hands":2,
        "tags":["Heavy","Reach"]
    },
    "Rapier":{
        "cost": 25,
        "damageRoll": function(character){ return Utility.rollDie(6)+character.getAbilityModifier(this.ability); },
        "damageType": "Piercing",
        "weight":2,
        "ability":"Strength",
        "proficiencies":["MartialWeapons","Rapiers"],
        "hands":1,
        "tags":["Finesse"]
    },
    "Scimitar":{
        "cost": 25,
        "damageRoll": function(character){ return Utility.rollDie(6)+character.getAbilityModifier(this.ability); },
        "damageType": "Slashing",
        "weight":3,
        "ability":"Strength",
        "proficiencies":["MartialWeapons","Scimitars"],
        "hands":1,
        "tags":["Finesse","Light"]
    },
    "Shortsword":{
        "cost": 10,
        "damageRoll": function(character){ return Utility.rollDie(4)+character.getAbilityModifier(this.ability); },
        "damageType": "Piercing",
        "weight":2,
        "ability":"Strength",
        "proficiencies":["MartialWeapons","Swords"],
        "hands":1,
        "tags":["Finesse","Light"]
    },
    "Trident":{
        "cost": 5,
        "damageRoll": function(character){ return Utility.rollDie(8)+character.getAbilityModifier(this.ability); },
        "damageType": "Piercing",
        "weight":4,
        "ability":"Strength",
        "proficiencies":["MartialWeapons","Swords"],
        "hands":1.5,
        "tags":[]
    },
    "WarPick":{
        "cost": 5,
        "damageRoll": function(character){ return Utility.rollDie(8)+character.getAbilityModifier(this.ability); },
        "damageType": "Piercing",
        "weight":2,
        "ability":"Strength",
        "proficiencies":["MartialWeapons","WarPicks"],
        "hands":1,
        "tags":[]
    },
    "Warhammer":{
        "cost": 15,
        "damageRoll": function(character){ return Utility.rollDie(8)+character.getAbilityModifier(this.ability); },
        "damageType": "Bludgeoning",
        "weight":2,
        "ability":"Strength",
        "proficiencies":["MartialWeapons","WarPicks"],
        "hands":1.5,
        "tags":[]
    },
    "Whip":{
        "cost": 2,
        "damageRoll": function(character){ return Utility.rollDie(4)+character.getAbilityModifier(this.ability); },
        "damageType": "Slashing",
        "weight":3,
        "ability":"Strength",
        "proficiencies":["MartialWeapons","Whips"],
        "hands":1,
        "tags":["Finesse","Reach"]
    },
    "Blowgun":{
        "cost": 10,
        "damageRoll": function(character){ return Utility.rollDie(1)+character.getAbilityModifier(this.ability); },
        "damageType": "Piercing",
        "weight":1,
        "ability":"Dexterity",
        "proficiencies":["MartialWeapons","Blowguns"],
        "hands":1,
        "tags":["Loading"]
    },
    "HandCrossbow":{
        "cost": 75,
        "damageRoll": function(character){ return Utility.rollDie(6)+character.getAbilityModifier(this.ability); },
        "damageType": "Piercing",
        "weight":3,
        "ability":"Dexterity",
        "proficiencies":["MartialWeapons","HandCrossbows"],
        "hands":1,
        "tags":["Loading","Light"]
    },
    "HeavyCrossbow":{
        "cost": 50,
        "damageRoll": function(character){ return Utility.rollDie(10)+character.getAbilityModifier(this.ability); },
        "damageType": "Piercing",
        "weight":18,
        "ability":"Dexterity",
        "proficiencies":["MartialWeapons","HeavyCrossbows"],
        "hands":2,
        "tags":["Loading","Heavy"]
    },
    "Longbow":{
        "cost": 50,
        "damageRoll": function(character){ return Utility.rollDie(8)+character.getAbilityModifier(this.ability); },
        "damageType": "Piercing",
        "weight":2,
        "ability":"Dexterity",
        "proficiencies":["MartialWeapons","HeavyCrossbows"],
        "hands":2,
        "tags":["Heavy"]
    },
    "Net":{
        "cost": 1,
        "damageRoll": function(character){ return 0; },
        "damageType": "Entangle",
        "weight":3,
        "ability":"Dexterity",
        "proficiencies":["MartialWeapons","Nets"],
        "hands":1,
        "tags":[]
    },
    //MONSTER WEAPONS
    "BadgerClaws":{
        "cost": 1,
        "damageRoll": function(character){ return Utility.rollDiceAndTally(2,4)+character.getAbilityModifier(this.ability); },
        "damageType": "Slashing",
        "weight":0,
        "ability":"Strength",
        "proficiencies":["BadgerClaws"],
        "hands":1,
        "tags":[]
        //TODO: not equippable!
    },
    "BatTeeth":{
        "cost": 1,
        "damageRoll": function(character){ return Utility.rollDie(6)+character.getAbilityModifier(this.ability); },
        "damageType": "Piercing",
        "weight":0,
        "ability":"Strength",
        "proficiencies":["BatTeeth"],
        "hands":1,//TODO: other equipment slots?
        "tags":[]
        //TODO: not equippable!
        //TODO: not droppable! (unless bat teeth?)
    }
};

function checkWeaponProficiency(character, weapon){
    var table={};
    for(var index in weapon.proficiencies){
        table[weapon.proficiencies[index]]=true;
    }
    for(var index in character.proficiencies.weapons){
        if(table[character.proficiencies.weapons[index]]){
            return true;
        }
    }
    return false;
}
