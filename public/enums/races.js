var raceTable = {
    "Dwarf":1,
    "Elf":1,
    "Gnome":1,
    "Halfling":1,
    "Half-Elf":1,
    "Half-Orc":1,
    "Human":1
};

var races = {
    "Dwarf":{
        "applyAbilityBonuses":function(character){
            character.abilities["Constitution"]+=2;
        }
    },
    "Elf":{
        "applyAbilityBonuses":function(character){
            character.abilities["Dexterity"]+=2;
        }
    },
    "Gnome":{
        "applyAbilityBonuses":function(character){
            character.abilities["Intelligence"]+=2;
        }
    },
    "Halfling":{
        "applyAbilityBonuses":function(character){
            character.abilities["Dexterity"]+=2;
        }
    },
    "Half-Elf":{
        "applyAbilityBonuses":function(character){
            character.abilities["Charisma"]+=2;
            var x=["Strength", "Constitution", "Dexterity", "Intelligence", "Wisdom"];
            x.sort(function(){return Utility.rollDie(3)-2;});
            character.abilities[x[0]]++;
            character.abilities[x[1]]++;
        }
    },
    "Half-Orc":{
        "applyAbilityBonuses":function(character){
            character.abilities["Strength"]+=2;
            character.abilities["Constitution"]++;
        }
    },
    "Human":{
        "applyAbilityBonuses":function(character){
            for(var key in abilities){
                character.abilities[key]++;
            }
        }
    }
};
