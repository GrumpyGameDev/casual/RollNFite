var autoplayTimerId = null;

function autoplay(){
    var index = Math.floor(Math.random() * ($('.autoplay').length));
    var control = $('.autoplay').slice(index,index+1);
    control.click();
}

function toggleAutoplay(){
    if(autoplayTimerId){
        clearInterval(autoplayTimerId);
        autoplayTimerId=null;
    }else{
        autoplayTimerId = setInterval(autoplay,100);
    }
}
