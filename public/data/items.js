function createTrophyItem(trophyType){
    return {
        "itemType":"Trophy",
        "trophyType":trophyType
    };
}
function createWeaponItem(weaponType){
    return {
        "itemType":"Weapon",
        "weaponType":weaponType
    };
}
function createArmorItem(armorType){
    return {
        "itemType":"Armor",
        "armorType":armorType
    };
}
function createNothingItem(){
    return {
        "itemType":"Nothing"
    };
}
function createTwoHandedItem(){
    return {
        "itemType":"TwoHands"
    };
}
function duplicateItem(item){
    var result={};
    for(var key in item){
        result[key]=item[key];
    }
    return result;
}
function getItemName(item){
    if(item.itemType=="Weapon"){
        return item.weaponType;
    }else if(item.itemType=="Armor"){
        return item.armorType;
    }else if(item.itemType=="Trophy"){
        return item.trophyType;
    }else if(item.itemType=="Nothing"){
        return "Nothing"
    }else if(item.itemType=="TwoHands"){
        return "(using two hands)"
    }
}