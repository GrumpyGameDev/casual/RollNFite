var tracker = {
    statistics : {},
    track: function(identifier,suppressCharacterTracking){
        suppressCharacterTracking = suppressCharacterTracking || false;
        if(!suppressCharacterTracking){
            game.character.track(identifier);
        }
        this.statistics[identifier] = this.statistics[identifier] || 0;
        this.statistics[identifier]++;
    }
};