
function showStatistics(sortType){
    sortType = sortType || "Event";
    tracker.track("Viewed Statistics");
    var content = "<h1>Statistics</h1>";
    content += "<table class='table'>";
    content += "<thead><tr><th><a href='#' onclick='showStatistics(\"Event\")'>Event</a></th><th><a href='#' onclick='showStatistics(\"Count\")'>Count</a></th></tr></thead>";
    var report = [];
    for(var key in tracker.statistics){
        report.push({event:key,count:tracker.statistics[key]});
    }
    if(sortType=="Event"){
        report.sort(function(first,second){
            if(first.event==second.event){
                return 0;
            } else if(first.event>second.event){
                return 1;
            } else if(first.event<second.event){
                return -1;
            }
        });
    }else{
        report.sort(function(first,second){
            if(first.count==second.count){
                return 0;
            } else if(first.count>second.count){
                return 1;
            } else if(first.count<second.count){
                return -1;
            }
        });
    }
    content += "<tbody>";
    for(var index in report){
        var item = report[index];
        content += "<tr><td>"+item.event+"</td><td>"+item.count+"</td></tr>";
    }
    content += "</tbody></table>";
    $('#main')
        .empty()
        .append(content);
}

