var monsterTable = {
    "Kobold":1,
    "GiantBadger":0,
    "GiantBat":0
};
var monsters={
    "GiantBat":{
        "abilities":{
            Strength:15,
            Dexterity:16,
            Constitution:11,
            Intelligence:2,
            Wisdom:12,
            Charisma:6,
        },
        "imageUri":"images/bat.png",
        "proficiencies":{
            "weapons":["BatTeeth"]
        },
        "weapon":createWeaponItem("BatTeeth"),
        "getAbilityModifier":function(ability){
            return Math.floor((this.abilities[ability]-10)/2);
        },
        "getAttackModifier": function(){
            var modifier = 0;
            var weapon = weapons[this.weapon.weaponType];
            modifier += this.getAbilityModifier(weapon.ability);
            if(checkWeaponProficiency(this,weapon)){
                modifier += 2;//TODO: does this change based on monster?
            }
            return modifier;
        },
        "getArmorClass":function(){
            return 13;
        },
        "getMaximumHitPoints":function(){
            return 13;
        },
        "getExperiencePoints":function(){
            return 50;
        },
        "speed":30,
        "languages":[],
        "alignment":"Unaligned",
        "challenge":0.25
        //blindfighting 60
        //passive perception 11
        //actions: bite
    },
    "GiantBadger":{
        "abilities":{
            Strength:13,
            Dexterity:10,
            Constitution:15,
            Intelligence:2,
            Wisdom:12,
            Charisma:5,
        },
        "imageUri":"images/bear-head.png",
        "proficiencies":{
            "weapons":["BadgerClaws"]
        },
        "weapon":createWeaponItem("BadgerClaws"),
        "getAbilityModifier":function(ability){
            return Math.floor((this.abilities[ability]-10)/2);
        },
        "getAttackModifier": function(){
            var modifier = 0;
            var weapon = weapons[this.weapon.weaponType];
            modifier += this.getAbilityModifier(weapon.ability);
            if(checkWeaponProficiency(this,weapon)){
                modifier += 2;//TODO: does this change based on monster?
            }
            return modifier;
        },
        "getArmorClass":function(){
            return 10;
        },
        "getMaximumHitPoints":function(){
            return 13;
        },
        "getExperiencePoints":function(){
            return 50;
        },
        "speed":30,
        "languages":[],
        "alignment":"Unaligned",
        "challenge":0.25
        //darkvision 30
        //passive perception 11
        //actions: multiattack, bite, claws
    },
    "Kobold":{
        "abilities":{
            Strength:7,
            Dexterity:15,
            Constitution:9,
            Intelligence:8,
            Wisdom:7,
            Charisma:8,
        },
        "imageUri":"images/velociraptor.png",
        "proficiencies":{
            "weapons":["Dagger","Slings"]
        },
        "weapon":createWeaponItem("Dagger"),
        "getAbilityModifier":function(ability){
            return Math.floor((this.abilities[ability]-10)/2);
        },
        "getAttackModifier": function(){
            var modifier = 0;
            var weapon = weapons[this.weapon.weaponType];
            modifier += this.getAbilityModifier(weapon.ability);
            if(checkWeaponProficiency(this,weapon)){
                modifier += 2;//TODO: does this change based on monster?
            }
            return modifier;
        },
        "getArmorClass":function(){
            return 12;
        },
        "getMaximumHitPoints":function(){
            return 5;
        },
        "getExperiencePoints":function(){
            return 25;
        },
        "speed":30,
        "languages":["Common","Draconic"],
        "alignment":"LawfulEvil",
        "challenge":0.125
        //darkvision 60
        //passive perception 8
        //sunlight sensitivity
        //pack tactics
        //actions: dagger, sling
    }
};