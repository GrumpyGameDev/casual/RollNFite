var branchDirections = ["Back","Left","Ahead","Right"];

var encounterTable = {
    "Nothing":1,
    "Monster":1
};

function createBranch(branches,previousBranchId){
    var branch = {
        "exits":{},
        "monsters":[],
        "inventory":[],
        "addItem": function(item){
            this.inventory.push(duplicateItem(item));
        },
        "hasItems":function(){
            return this.inventory.length>0;
        },
        "removeItem":function(itemIndex){
            return this.inventory.splice(itemIndex,1)[0];
        }
    };
    branches.push(branch);
    if(previousBranchId!=null){
        branch.exits["Back"]=previousBranchId;
        var encounterType = Utility.generate(encounterTable);
        if(encounterType=="Monster"){
            var monsterType = Utility.generate(monsterTable);
            var monster = monsters[monsterType];
            branch.monsters.push({
                monsterType:monsterType,
                hitPoints:monster.getMaximumHitPoints()
            });
        }
    }else{
        //initial branch
    }
    var branchCount = Utility.rollDiceAndTally(2,2)-1;
    var directions = ["Left","Ahead","Right"].sort(function(){ return Utility.rollDie(3)-2; });
    for(var index=0;index<branchCount;++index){
        var direction=directions[index];
        branch.exits[direction]=branches.length;
        branches.push(null);
    }
    return branch;
}