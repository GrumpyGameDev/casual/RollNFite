var classTable={
    "Barbarian":1,
    "Bard":1,
    "Cleric":1,
    "Druid":1,
    "Fighter":1,
    "Monk":1,
    "Paladin":1,
    "Ranger":1,
    "Rogue":1,
    "Sorcerer":1,
    "Warlock":1,
    "Wizard":1,
};

var classes = {
    "Barbarian":{
        "hitDieSize":12,
        "generateStartingEquipment": function(character){
            character.equipItem("OnHand", createWeaponItem("Greataxe"));
            character.equipItem("OffHand", createTwoHandedItem());
            // (a) a Greataxe or (b) any martial melee weapon
            // (a) two handaxes or (b) any simple weapon
            // An explorer’s pack and four javelins
        },
        "generateProficiencies": function(character){
            character.addProficiency("armor","LightArmor");
            character.addProficiency("armor","MediumArmor");
            character.addProficiency("armor","Shields");
            character.addProficiency("weapons","SimpleWeapons");
            character.addProficiency("weapons","MartialWeapons");
            character.addProficiency("savingThrows","Strength");
            character.addProficiency("savingThrows","Constitution");
        }
    },
    "Bard":{
        "hitDieSize":8,
        "generateStartingEquipment": function(character){
            character.equipItem("OnHand",
                createWeaponItem(Utility.pick(
                    ["Club",
                    "Dagger",
                    "Greatclub",
                    "Handaxe",
                    "Javelin",
                    "Light Hammer",
                    "Mace",
                    "Quarterstaff",
                    "Sickle",
                    "Spear",
                    "Rapier",
                    "Longsword"],1)[0]));
            // (a) a Rapier, (b) a Longsword, or (c) any simple weapon
            // (a) a Diplomat's Pack or (b) an Entertainer's Pack
            // (a) a lute or (b) any other musical instrument
            // Leather Armor, and a Dagger
        },
        "generateProficiencies": function(character){
            character.addProficiency("armor","LightArmor");
            character.addProficiency("weapons","SimpleWeapons");
            character.addProficiency("weapons","HandCrossbows");
            character.addProficiency("weapons","LongSwords");
            character.addProficiency("weapons","Rapiers");
            character.addProficiency("weapons","ShortSwords");
            //TODO: three musical instruments
            var skills = Utility.pick([
                "Acrobatics",
                "Animal Handling",
                "Arcana",
                "Athletics",
                "Deception",
                "History",
                "Insight",
                "Intimidation",
                "Investigation",
                "Medicine",
                "Nature",
                "Perception",
                "Performance",
                "Persuasion",
                "Religion",
                "SleightOfHand",
                "Stealth",
                "Survival"
            ],3);
            for(var index in skills){
                character.addProficiency("skills",skills[index]);
            }
        }
    },
    "Cleric":{
        "hitDieSize":8,
        "generateStartingEquipment": function(character){
            character.equipItem("OnHand", createWeaponItem("Mace"));
            // (a) a mace or (b) a Warhammer (if proficient)
            // (a) Scale Mail, (b) Leather Armor, or (c) Chain Mail (if proficient)
            // (a) a Light Crossbow and 20 bolts or (b) any simple weapon
            // (a) a Priest's Pack or (b) an Explorer's Pack
            // A Shield and a holy Symbol
        },
        "generateProficiencies": function(character){
            character.addProficiency("armor","LightArmor");
            character.addProficiency("armor","MediumArmor");
            character.addProficiency("armor","Shields");
            character.addProficiency("weapons","SimpleWeapons");
            character.addProficiency("savingThrows","Wisdom");
            character.addProficiency("savingThrows","Charisma");
            var skills = Utility.pick([
                "History",
                "Religion",
                "Insight",
                "Medicine",
                "Persuasion"
            ],2);
            for(var index in skills){
                character.addProficiency("skills",skills[index]);
            }
        }
    },
    "Druid":{
        "hitDieSize":8,
        "generateStartingEquipment": function(character){
            character.equipItem("OnHand", createWeaponItem("Scimitar"));
            // (a) a Wooden Shield or (b) any simple weapon
            // (a) a Scimitar or (b) any simple melee weapon
            // Leather Armor, an Explorer's Pack, and a druidic focus
        },
        "generateProficiencies": function(character){
            character.addProficiency("armor","LightArmor");
            character.addProficiency("armor","MediumArmor");
            character.addProficiency("armor","Shields");
            character.addProficiency("weapons","Clubs");
            character.addProficiency("weapons","Daggers");
            character.addProficiency("weapons","Darts");
            character.addProficiency("weapons","Javelins");
            character.addProficiency("weapons","Maces");
            character.addProficiency("weapons","Quarterstaves");
            character.addProficiency("weapons","Scimitars");
            character.addProficiency("weapons","Sickles");
            character.addProficiency("weapons","Slings");
            character.addProficiency("weapons","Spears");
            character.addProficiency("savingThrows","Wisdom");
            character.addProficiency("savingThrows","Intelligence");
            var skills = Utility.pick([
                "Animal Handling",
                "Arcana",
                "Insight",
                "Medicine",
                "Nature",
                "Perception",
                "Religion",
                "Survival"
            ],2);
            for(var index in skills){
                character.addProficiency("skills",skills[index]);
            }
        }
    },
    "Fighter":{
        "hitDieSize":10,
        "generateStartingEquipment": function(character){
            character.equipItem("OnHand", createWeaponItem("Longsword"));
            // (a) Chain Mail or (b) leather, Longbow, and 20 Arrows
            // (a) a martial weapon and a Shield or (b) two Martial Weapons
            // (a) a Light Crossbow and 20 bolts or (b) two handaxes
            // (a) a Dungeoneer's Pack or (b) an Explorer's Pack
        },
        "generateProficiencies": function(character){
            character.addProficiency("armor","LightArmor");
            character.addProficiency("armor","MediumArmor");
            character.addProficiency("armor","HeavyArmor");
            character.addProficiency("armor","Shields");
            character.addProficiency("weapons","SimpleWeapons");
            character.addProficiency("weapons","MartialWeapons");
            var skills = Utility.pick([
                "Animal Handling",
                "Athletics",
                "Acrobatics",
                "History",
                "Insight",
                "Intimidation",
                "Perception",
                "Survival"
            ],2);
            for(var index in skills){
                character.addProficiency("skills",skills[index]);
            }
        }
    },
    "Monk":{
        "hitDieSize":8,
        "generateStartingEquipment": function(character){
            character.equipItem("OnHand", createWeaponItem("Quarterstaff"));
            // (a) a Shortsword or (b) any simple weapon
            // (a) a Dungeoneer's Pack or (b) an Explorer's Pack
            // 10 darts
        },
        "generateProficiencies": function(character){
            character.addProficiency("weapons","SimpleWeapons");
            character.addProficiency("weapons","ShortSwords");
            //TODO: 1 artisan's tools or musical instrument
            var skills = Utility.pick([
                "Acrobatics",
                "Athletics",
                "History",
                "Insight",
                "Religion",
                "Stealth"
            ],2);
            for(var index in skills){
                character.addProficiency("skills",skills[index]);
            }
        }
    },
    "Paladin":{
        "hitDieSize":10,
        "generateStartingEquipment": function(character){
            character.equipItem("OnHand", createWeaponItem("Longsword"));
            // (a) a martial weapon and a Shield or (b) two Martial Weapons
            // (a) five javelins or (b) any simple melee weapon
            // (a) a Priest's Pack or (b) an Explorer's Pack
            // Chain Mail and a holy Symbol
        },
        "generateProficiencies": function(character){
            character.addProficiency("armor","LightArmor");
            character.addProficiency("armor","MediumArmor");
            character.addProficiency("armor","HeavyArmor");
            character.addProficiency("armor","Shields");
            character.addProficiency("weapons","SimpleWeapons");
            character.addProficiency("weapons","MartialWeapons");
            var skills = Utility.pick([
                "Athletics",
                "Insight",
                "Intimidation",
                "Medicine",
                "Persuasion",
                "Religion"
            ],2);
            for(var index in skills){
                character.addProficiency("skills",skills[index]);
            }
        }
    },
    "Ranger":{
        "hitDieSize":10,
        "generateStartingEquipment": function(character){
            character.equipItem("OnHand", createWeaponItem("Longsword"));
            // (a) Scale Mail or (b) Leather Armor
            // (a) two shortswords or (b) two simple Melee Weapons
            // (a) a Dungeoneer's Pack or (b) an Explorer's Pack
            // A Longbow and a Quiver of 20 Arrows
        },
        "generateProficiencies": function(character){
            character.addProficiency("armor","LightArmor");
            character.addProficiency("armor","MediumArmor");
            character.addProficiency("armor","Shields");
            character.addProficiency("weapons","SimpleWeapons");
            character.addProficiency("weapons","MartialWeapons");
            var skills = Utility.pick([
                "Animal Handling",
                "Athletics",
                "Insight",
                "Investigation",
                "Nature",
                "Perception",
                "Stealth",
                "Survival"
            ],3);
            for(var index in skills){
                character.addProficiency("skills",skills[index]);
            }
        }
    },
    "Rogue":{
        "hitDieSize":8,
        "generateStartingEquipment": function(character){
            character.equipItem("OnHand", createWeaponItem("Rapier"));
            //(a) a Rapier or (b) a Shortsword
            //(a) a Shortbow and Quiver of 20 Arrows or (b) a Shortsword
            //(a) a Burglar's Pack, (b) a Dungeoneer's Pack, or (c) an Explorer's Pack
            //Leather Armor, two daggers, and Thieves' Tools
        },
        "generateProficiencies": function(character){
            character.addProficiency("armor","LightArmor");
            character.addProficiency("weapons","SimpleWeapons");
            character.addProficiency("weapons","HandCrossbows");
            character.addProficiency("weapons","LongSwords");
            character.addProficiency("weapons","Rapiers");
            character.addProficiency("weapons","ShortSwords");
            character.addProficiency("tools","ThievesTools");
            var skills = Utility.pick([
                "Acrobatics",
                "Athletics",
                "Deception",
                "Insight",
                "Intimidation",
                "Investigation",
                "Perception",
                "Performance",
                "Persuasion",
                "SleightOfHand",
                "Stealth"
            ],4);
            for(var index in skills){
                character.addProficiency("skills",skills[index]);
            }
        }
    },
    "Sorcerer":{
        "hitDieSize":6,
        "generateStartingEquipment": function(character){
            character.equipItem("OnHand", createWeaponItem("Dagger"));
            // (a) a Light Crossbow and 20 bolts or (b) any simple weapon
            // (a) a Component pouch or (b) an arcane focus
            // (a) a Dungeoneer's Pack or (b) an Explorer's Pack
            // Two daggers
        },
        "generateProficiencies": function(character){
            character.addProficiency("weapons","Daggers");
            character.addProficiency("weapons","Darts");
            character.addProficiency("weapons","Slings");
            character.addProficiency("weapons","QuarterStaves");
            character.addProficiency("weapons","LightCrossbows");
            var skills = Utility.pick([
                "Arcana",
                "Deception",
                "Insight",
                "Intimidation",
                "Investigation",
                "Persuasion",
                "Religion"
            ],2);
            for(var index in skills){
                character.addProficiency("skills",skills[index]);
            }
        }
    },
    "Warlock":{
        "hitDieSize":8,
        "generateStartingEquipment": function(character){
            character.equipItem("OnHand", createWeaponItem("Quarterstaff"));
            // (a) a Light Crossbow and 20 bolts or (b) any simple weapon
            // (a) a Component pouch or (b) an arcane focus
            // (a) a Scholar's Pack or (b) a Dungeoneer's Pack
            // Leather Armor, any simple weapon, and two daggers
        },
        "generateProficiencies": function(character){
            character.addProficiency("armor","LightArmor");
            character.addProficiency("weapons","SimpleWeapons");
            var skills = Utility.pick([
                "Arcana",
                "Deception",
                "History",
                "Intimidation",
                "Investigation",
                "Nature",
                "Religion"
            ],2);
            for(var index in skills){
                character.addProficiency("skills",skills[index]);
            }
        }
    },
    "Wizard":{
        "hitDieSize":6,
        "generateStartingEquipment": function(character){
            character.equipItem("OnHand", createWeaponItem("Quarterstaff"));
            // (a) a Quarterstaff or (b) a Dagger
            // (a) a Component pouch or (b) an arcane focus
            // (a) a Scholar's Pack or (b) an Explorer's Pack
            // A Spellbook
        },
        "generateProficiencies": function(character){
            character.addProficiency("weapons","Daggers");
            character.addProficiency("weapons","Darts");
            character.addProficiency("weapons","Slings");
            character.addProficiency("weapons","QuarterStaves");
            character.addProficiency("weapons","LightCrossbows");
            var skills = Utility.pick([
                "Arcana",
                "History",
                "Insight",
                "Investigation",
                "Medicine",
                "Religion"
            ],2);
            for(var index in skills){
                character.addProficiency("skills",skills[index]);
            }
        }
    }
};