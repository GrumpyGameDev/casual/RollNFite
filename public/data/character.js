function createCharacter(){
    var character = {
        "race":Utility.generate(raceTable),
        "class":Utility.generate(classTable),
        "abilities":{},
        "hitDice":[],
        "branches":[],
        "branchId":0,
        "messages":[],
        "experiencePoints":0,
        "inventory":[],
        "equippedItems":{
            "Body":createArmorItem("None"),
            "OffHand":createNothingItem(),
            "OnHand":createNothingItem()
        },
        "proficiencies":{
            "armor":["NoArmor"],
            "savingThrows":[],
            "weapons":["Unarmed"],
            "tools":[],
            "skills":[]
        },
        "track":function(identifier){
            var quest = this.quests[identifier];
            if(quest!=null){
                quest.count++;
                if(quest.count>=quest.goal){
                    quest.reward(this);
                }
            }
        },
        "quests":{
            "Killed Kobold!":{//TODO: name this more sanely!
                "goal":5,
                "count":0,
                "reward":function(character){
                    character.addItem(createTrophyItem("Kobold Badge"));
                    character.messages.push("You've earned a Kobold Badge!");
                    tracker.track("Kobold Badge Earned!");
                    character.quests["Killed Kobold!"].count = 0;
                    character.quests["Killed Kobold!"].goal *= 2;
                }
            }
        },
        "equipItem":function(slot,item){
            var oldItem = this.equippedItems[slot];
            this.equippedItems[slot]=item;
            return oldItem;
        },
        "unequipItem":function(slot){
            var oldItem = this.equippedItems[slot];
            if(slot=="Body"){
                this.equippedItems[slot]=createArmorItem("None");
            }else{
                this.equippedItems[slot]=createNothingItem();
            }
            return oldItem;
        },
        "addProficiency":function(proficiencyType, proficiency){
            var found = false;
            for(var index in this.proficiencies[proficiencyType]){
                if(this.proficiencies[proficiencyType][index]==proficiency){
                    found=true;
                    break;
                }
            }
            if(!found){
                this.proficiencies[proficiencyType].push(proficiency);
            }
        },
        "addItem": function(item){
            this.inventory.push(duplicateItem(item));
        },
        "hasItems":function(){
            return this.inventory.length>0;
        },
        "removeItem":function(itemIndex){
            return this.inventory.splice(itemIndex,1)[0];
        },
        "getLevelId":function(){
            return this.hitDice.length-1;
        },
        "getAbilityModifier":function(ability){
            return Math.floor((this.abilities[ability]-10)/2);
        },
        "getMaximumHitPoints":function(){
            var total = 0;
            for(var index in this.hitDice){
                total+=this.hitDice[index];
                total+=this.getAbilityModifier("Constitution");
            }
            return total;
        },
        "getAttackModifier": function(){
            var modifier = 0;
            var weapon = weapons[this.equippedItems.OnHand.weaponType];
            modifier += this.getAbilityModifier(weapon.ability);
            if(checkWeaponProficiency(this,weapon)){
                var currentLevel = levels[this.getLevelId()];
                modifier += currentLevel.proficiencyBonus;
            }
            return modifier;
        },
        "getArmorClass":function(){
            var total = 0;
            for(var key in this.equippedItems){
                var item = this.equippedItems[key];
                if(item.itemType=="Armor"){
                    var armor = armors[item.armorType];
                    if(key=="Body"){
                        total = armor.baseArmorClass;
                        if(armor.useDexterityModifier){
                            total+= Math.min(this.getAbilityModifier("Dexterity"), armor.maximumDexterityModifier);
                        }
                    }else{
                        total += armor.bonusArmorClass;
                    }
                }
            }
            return total;
        },
        "addExperiencePoints":function(experiencePoints){
            this.experiencePoints += experiencePoints;
            this.messages.push("You earned "+experiencePoints+" XP!");
            var nextLevel = levels[this.getLevelId()+1];
            if(nextLevel!=null){
                if(this.experiencePoints>nextLevel.experiencePoints){
                    var hitDieSize = classes[this.class].hitDieSize;
                    var hitDieRoll = Utility.rollDie(hitDieSize);
                    this.hitDice.push(Math.max(hitDieRoll,Math.floor(hitDieSize/2+1)));
                    var currentLevel = levels[this.getLevelId()];
                    this.messages.push("You reached level "+currentLevel.level+"!");
                }
            }
        }
    };
    createBranch(character.branches);
    for(var key in abilities){
        var dice = Utility.rollDice(4,6);
        dice.sort(function(first,second){return second-first});
        character.abilities[key]=dice[0]+dice[1]+dice[2];
    }
    races[character.race].applyAbilityBonuses(character);
    character.hitDice.push(classes[character.class].hitDieSize);//first level, get max hp!
    classes[character.class].generateProficiencies(character);
    classes[character.class].generateStartingEquipment(character);
    character.hitPoints = character.getMaximumHitPoints();
    return character;
}

