function moveToBranch(branchId,message,predicate){
    var oldBranchId= game.character.branchId;
    if(game.character.branches[branchId]==null){
        game.character.branches[branchId] = createBranch(game.character.branches, oldBranchId);
    }
    game.character.branchId=branchId;
    if(message!=null){
        game.character.messages.push(message);
    }
    if(predicate){
        predicate();
    }
    tracker.track("Moved!");
    showGame();
}

function flee(){
    var exits = game.character.branches[game.character.branchId].exits;
    var table = {};
    for(var key in exits){
        table[exits[key]]=1;
    }
    tracker.track("Fleeing!");
    moveToBranch(Utility.generate(table),"You flee in a random direction!",doMonsterAttacks);
}

function doMonsterAttacks(){
    var branch = game.character.branches[game.character.branchId];
    var character = game.character;
    for(var index in branch.monsters){
        var attacker = branch.monsters[index];
        var monster = monsters[attacker.monsterType];
        var attackRoll = monster.getAttackModifier()+Utility.rollDie(20);
        character.messages.push(attacker.monsterType + " attacks!");
        if(attackRoll>=character.getArmorClass()){
            var weapon = weapons[monster.weapon.weaponType];
            var damage = Math.max(0,weapon.damageRoll(monster));
            character.messages.push(attacker.monsterType + " hits you for "+damage+" HP!");
            character.hitPoints-=damage;
            if(character.hitPoints<=0){
                tracker.track("Death!");
                tracker.track("Killed by "+attacker.monsterType+"!");
                character.messages.push("You died. Killed by a "+attacker.monsterType+".");
            }
        }else{
            character.messages.push(attacker.monsterType + " misses!");
        }
    }
}

function attackMonster(monsterIndex){
    var branch = game.character.branches[game.character.branchId];
    var defender = branch.monsters[monsterIndex];
    var monster = monsters[defender.monsterType];
    var character = game.character;
    var attackRoll = character.getAttackModifier()+Utility.rollDie(20);
    character.messages.push("You attack!");
    tracker.track("Attacked!");
    if(attackRoll>=monster.getArmorClass()){
        tracker.track("Hit!");
        var weapon = weapons[character.equippedItems.OnHand.weaponType];
        var damage = Math.max(0,weapon.damageRoll(character));
        character.messages.push("You hit for "+damage+" HP!");
        defender.hitPoints-=damage;
        if(defender.hitPoints<=0){
            tracker.track("Killed "+defender.monsterType+"!");
            character.messages.push("You killed the "+defender.monsterType+"!");
            branch.monsters.splice(monsterIndex,1);
            character.addExperiencePoints(monster.getExperiencePoints());
            branch.addItem(monster.weapon);
        }else{
            doMonsterAttacks();
        }
    }else{
        tracker.track("Missed!");
        character.messages.push("You miss!");
        doMonsterAttacks();
    }
    showGame();
}

function getCharacterBranch(){
    return game.character.branches[game.character.branchId];
}

function pickUpItem(itemIndex){
    tracker.track("Picked Up Item");
    var character = game.character;
    var branch = getCharacterBranch();
    var item = branch.removeItem(itemIndex);
    character.addItem(item);
    character.messages.push("You pick up the "+getItemName(item)+".");
    showGame();
}

function showBranch(){
    var content = "<p>Branch #"+game.character.branchId+"</p>";
    var branch = game.character.branches[game.character.branchId];
    for(var index in game.character.messages){
        var message = game.character.messages[index];
        content+="<p>"+message+"</p>";
    }
    if(game.character.hitPoints>0){
        game.character.messages=[];
        if(branch.monsters.length>0){
            content += "<p>You are in combat!</p>"
            content += "<p>You have "+game.character.hitPoints+" HP!</p>"
            content += "<div class='list-group'>";
            for(var index in branch.monsters){
                var monsterInstance = branch.monsters[index];
                var monster = monsters[monsterInstance.monsterType];
                content += "<li class='list-group-item'><img src='"+monster.imageUri+"'/>"+monsterInstance.monsterType+" <a class='autoplay' href='#' onclick='attackMonster("+index+")'>Attack!</a></li>"
            }
            content += "<a href='#' class='autoplay list-group-item list-group-item-action' onclick='flee()'>Flee!</a>";
            content += "</div>";
        } else {
            if(branch.hasItems()){
                content += "<p>On the ground:</p>";
                content += "<div class='list-group'>"
                for(var index in branch.inventory){
                    var item = branch.inventory[index];
                    content += "<a href='#' class='autoplay list-group-item list-group-item-action' onclick='pickUpItem("+index+")'>"+getItemName(item)+"</a>";
                }
                content += "</div>"
            }
            content += "<p>Exits</p>";
            content += "<div class='list-group'>"
            for(var key in branch.exits){
                var branchId = branch.exits[key];
                content+="<a href='#' class='autoplay list-group-item list-group-item-action' onclick='moveToBranch("+branchId+",\"You move "+key+".\")'>"+key+"</a>";
            }
            content += "</div>"
        }
    }else{
        content+="<button id=\"newGame\" class=\"autoplay btn\" onclick=\"newGame()\">Restart</button>";
    }
    $('#main').append(content);
}

function showGame(){
    $('#main').empty();
    showBranch();
}
