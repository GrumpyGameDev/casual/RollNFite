function showCharacter(){
    tracker.track("Viewed Character");
    var content = "";
    var character = game.character;

    content += "<h1>Basics Stats</h1>";
    content += "<ul class='list-group'>";
    content += "<li class='list-group-item'>Race: "+character.race+"</li>";
    content += "<li class='list-group-item'>Class: "+character.class+"</li>";
    content += "<li class='list-group-item'>Level: "+levels[character.getLevelId()].level+"</li>";
    content += "<li class='list-group-item'>Experience Points: "+character.experiencePoints+"</li>";
    content += "<li class='list-group-item'>Hit Points: "+character.hitPoints+"</li>";
    content += "<li class='list-group-item'>Armor Class: "+character.getArmorClass()+"</li>";
    content += "</ul>";

    content += "<h1>Ability Scores</h1>";
    content += "<ul class='list-group'>";
    var abilityList = ["Strength","Dexterity","Constitution","Intelligence","Wisdom","Charisma"];
    for(var index in abilityList){
        var key = abilityList[index];
        content += "<li class='list-group-item'>"+key+" " + character.abilities[key] + "<span class='badge'>"+character.getAbilityModifier(key)+"</span></li>";
    }
    content += "</ul>"

    content += "<h1>Proficiencies</h1>";
    content += "<ul class='list-group'>";
    for(var proficiencyType in character.proficiencies){
        for(var index in character.proficiencies[proficiencyType]){
            var proficiency = character.proficiencies[proficiencyType][index];
            content += "<li class='list-group-item d-flex justify-content-between align-items-center'>"+proficiency+"<span class='badge badge-primary badge-pill'>"+proficiencyType+"</span></li>";
        }
    }
    content += "</ul>"

    content += "<h1>Equipped Items</h1>";
    content += "<ul class='list-group'>";
    for(var slot in character.equippedItems){
        var item = character.equippedItems[slot];
        content += "<li class='list-group-item d-flex justify-content-between align-items-center'>"+getItemName(item)+"<span class='badge badge-primary badge-pill'>"+slot+"</span></li>";
    }
    content += "</ul>"

    $('#main')
        .empty()
        .append(content);
}

