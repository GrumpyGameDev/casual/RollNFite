function showAbout(){
    var content = "";
    content += "<h1>Image Credits</h1>";
    content += "<p><a href='https://game-icons.net/delapouite/originals/velociraptor.html'>Velociraptor</a> by Delapouite under CC BY 3.0</p>";
    content += "<p><a href='https://game-icons.net/delapouite/originals/bear-head.html'>Bear Head</a> by Delapouite under CC BY 3.0</p>";
    //https://game-icons.net/delapouite/originals/bat.html
    //https://game-icons.net/delapouite/originals/rat.html
    $('#main')
        .empty()
        .append(content);
}