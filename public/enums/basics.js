var abilities = {
    "Charisma":{
        "abbr": "CHA"
    },
    "Constitution":{
        "abbr": "CON"
    },
    "Dexterity":{
        "abbr": "DEX"
    },
    "Intelligence":{
        "abbr": "INT"
    },
    "Strength":{
        "abbr": "STR"
    },
    "Wisdom":{
        "abbr": "WIS"
    }
};

