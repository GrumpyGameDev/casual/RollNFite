var Utility = {
    pick: function(items,count){
        count = count || 0;
        return items.sort(function(){return Utility.rollDie(3)-2;}).slice(0,count);
    },
    rollDie: function(dieSize){
        return Math.floor(Math.random() * dieSize)+1;
    },
    rollDice:function(dieCount,dieSize){
        var result = [];
        while(result.length<dieCount){
            result.push(Utility.rollDie(dieSize));
        }
        return result;
    },
    rollDiceAndTally:function(dieCount,dieSize){
        var dice=Utility.rollDice(dieCount,dieSize);
        var total = 0;
        for(var n in dice){
            total+=dice[n];
        }
        return total;
    },
    generate:function(table){
        var total = 0;
        for(var key in table){
            total+=table[key];
        }
        var generated = Utility.rollDie(total)-1;
        for(var key in table){
            generated-=table[key];
            if(generated<0){
                return key;
            }
        }
        throw "generate failed!";
    }
};